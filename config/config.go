package config

import (
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"

	TimeExpiredAt = time.Hour * 720
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	ServisHost string
	HTTPPort   string
	HTTPScheme string
	Domain     string

	DefaultOffset string
	DefaultLimit  string

	UserServiceHost string
	UserGRPCPort    string

	SELLServiceHost string
	SELLGRPCPort    string

	LOADServiceHost string
	LOADGRPCPort    string

	SecretKey string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("./.env"); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "ms_go_api_gateway"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.ServisHost = cast.ToString(getOrReturnDefaultValue("SERVICE_HOST", "localhost"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8001"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))
	config.Domain = cast.ToString(getOrReturnDefaultValue("DOMAIN", "localhost:8001"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("LIMIT", "0"))

	config.UserServiceHost = cast.ToString(getOrReturnDefaultValue("USER_SERVICE_HOST", "localhost"))
	config.UserGRPCPort = cast.ToString(getOrReturnDefaultValue("USER_GRPC_PORT", ":8101"))

	config.SELLServiceHost = cast.ToString(getOrReturnDefaultValue("SELL_SERVICE_HOST", "localhost"))
	config.SELLGRPCPort = cast.ToString(getOrReturnDefaultValue("SELL_GRPC_PORT", ":8102"))

	config.LOADServiceHost = cast.ToString(getOrReturnDefaultValue("LOAD_SERVICE_HOST", "localhost"))
	config.LOADGRPCPort = cast.ToString(getOrReturnDefaultValue("LOAD_GRPC_PORT", ":8103"))

	config.SecretKey = cast.ToString(getOrReturnDefaultValue("SECRET_KEY", "marketSystem"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
