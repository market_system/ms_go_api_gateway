package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/load_service"
)

// CreateArrivalProduct godoc
// @ID create_ArrivalProduct
// @Router /arrival/product [POST]
// @Summary  Create ArrivalProduct
// @Description Create ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body load_service.CreateArrivalProduct true "CreateArrivalProductRequestBody"
// @Success 200 {object} http.Response{data=load_service.ArrivalProduct} "GetArrivalProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateArrivalProduct(c *gin.Context) {
	var ArrivalProduct load_service.CreateArrivalProduct

	err := c.ShouldBindJSON(&ArrivalProduct)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ArrivalProductService().Create(c, &ArrivalProduct)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetArrivalProductByID godoc
// @ID get_ArrivalProduct_by_id
// @Router /arrival/product/{id} [GET]
// @Summary Get ArrivalProduct By ID
// @Description Get ArrivalProduct By ID
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=load_service.ArrivalProduct} "ArrivalProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetArrivalProductById(c *gin.Context) {

	arrival_pId := c.Param("id")

	resp, err := h.services.ArrivalProductService().GetByID(c, &load_service.ArrivalProductPrimaryKey{Id: arrival_pId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetArrivalProductList godoc
// @ID get_ArrivalProduct_list
// @Router /arrival/product [GET]
// @Summary Get ArrivalProduct s List
// @Description  Get ArrivalProduct s List
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=load_service.GetListArrivalProductResponse} "GetAllArrivalProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetArrivalProductList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ArrivalProductService().GetList(
		context.Background(),
		&load_service.GetListArrivalProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateArrivalProduct godoc
// @ID update_ArrivalProduct
// @Router /arrival/product/{id} [PUT]
// @Summary Update ArrivalProduct
// @Description Update ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body load_service.UpdateArrivalProduct true "UpdateArrivalProductRequestBody"
// @Success 200 {object} http.Response{data=load_service.ArrivalProduct} "ArrivalProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateArrivalProduct(c *gin.Context) {
	var arrival_p load_service.UpdateArrivalProduct

	arrival_p.Id = c.Param("id")

	err := c.ShouldBindJSON(&arrival_p)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ArrivalProductService().Update(
		c.Request.Context(),
		&arrival_p,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteArrivalProduct godoc
// @ID delete_arrival_product
// @Router /arrival/product/{id} [DELETE]
// @Summary Delete Arrival Product
// @Description Delete Arrival Product
// @Tags Arrival Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ArrivalProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteArrivalProduct(c *gin.Context) {

	arrival_pId := c.Param("id")

	resp, err := h.services.ArrivalProductService().Delete(
		c.Request.Context(),
		&load_service.ArrivalProductPrimaryKey{Id: arrival_pId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
