package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/sell_service"
)

// CreateSalesProduct godoc
// @ID create_SalesProduct
// @Router /sales/product [POST]
// @Summary  Create SalesProduct
// @Description Create SalesProduct
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sell_service.CreateSalesProduct true "CreateSalesProductRequestBody"
// @Success 200 {object} http.Response{data=sell_service.SalesProduct} "GetSalesProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSalesProduct(c *gin.Context) {
	var SalesProduct sell_service.CreateSalesProduct

	err := c.ShouldBindJSON(&SalesProduct)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SalesProductService().Create(c, &SalesProduct)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// Scan Barcode godoc
// @ID scan_barcode
// @Router /scan [Post]
// @Summary Scan Barcode
// @Description Post Sale Product With Scan Barcode
// @Tags ScanBarcode
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sell_service.ScanBarcode true "CreateSalesProductRequestBody"
// @Success 200 {object} http.Response{data=sell_service.SalesProduct} "GetSalesProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Scan(c *gin.Context) {

	var scan sell_service.ScanBarcode

	err := c.ShouldBindJSON(&scan)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SalesProductService().Scan(c, &scan)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetSalesProductByID godoc
// @ID get_SalesProduct_by_id
// @Router /sales/product/{id} [GET]
// @Summary Get SalesProduct By ID
// @Description Get SalesProduct By ID
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sell_service.SalesProduct} "SalesProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalesProductById(c *gin.Context) {

	sales_pId := c.Param("id")

	resp, err := h.services.SalesProductService().GetByID(c, &sell_service.SalesProductPrimaryKey{Id: sales_pId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetSalesProductList godoc
// @ID get_SalesProduct_list
// @Router /sales/product [GET]
// @Summary Get SalesProduct s List
// @Description  Get SalesProduct s List
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=sell_service.GetListSalesProductResponse} "GetAllSalesProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalesProductList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SalesProductService().GetList(
		context.Background(),
		&sell_service.GetListSalesProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateSalesProduct godoc
// @ID update_SalesProduct
// @Router /sales/product/{id} [PUT]
// @Summary Update SalesProduct
// @Description Update SalesProduct
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sell_service.UpdateSalesProduct true "UpdateSalesProductRequestBody"
// @Success 200 {object} http.Response{data=sell_service.SalesProduct} "SalesProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSalesProduct(c *gin.Context) {
	var sales_p sell_service.UpdateSalesProduct

	sales_p.Id = c.Param("id")

	err := c.ShouldBindJSON(&sales_p)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.SalesProductService().Update(
		c.Request.Context(),
		&sales_p,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteSales godoc
// @ID delete_sales
// @Router /sales/product/{id} [DELETE]
// @Summary Delete SalesProduct
// @Description Delete SalesProduct
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SalesProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSalesProduct(c *gin.Context) {

	sales_pId := c.Param("id")

	resp, err := h.services.SalesProductService().Delete(
		c.Request.Context(),
		&sell_service.SalesProductPrimaryKey{Id: sales_pId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
