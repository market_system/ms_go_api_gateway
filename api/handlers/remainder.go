package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/load_service"
)

// CreateRemainder godoc
// @ID create_Remainder
// @Router /remainder [POST]
// @Summary  Create Remainder
// @Description Create Remainder
// @Tags Remainder
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body load_service.CreateRemainder true "CreateRemainderRequestBody"
// @Success 200 {object} http.Response{data=load_service.Remainder} "GetRemainderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateRemainder(c *gin.Context) {
	var Remainder load_service.CreateRemainder

	err := c.ShouldBindJSON(&Remainder)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.RemainderService().Create(c, &Remainder)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetRemainderByID godoc
// @ID get_Remainder_by_id
// @Router /remainder/{id} [GET]
// @Summary Get Remainder By ID
// @Description Get Remainder By ID
// @Tags Remainder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=load_service.Remainder} "RemainderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRemainderById(c *gin.Context) {

	remainderId := c.Param("id")

	resp, err := h.services.RemainderService().GetByID(c, &load_service.RemainderPrimaryKey{Id: remainderId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetRemainderList godoc
// @ID get_Remainder_list
// @Router /remainder [GET]
// @Summary Get Remainder s List
// @Description  Get Remainder s List
// @Tags Remainder
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=load_service.GetListRemainderResponse} "GetAllRemainderResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRemainderList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.RemainderService().GetList(
		context.Background(),
		&load_service.GetListRemainderRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateRemainder godoc
// @ID update_Remainder
// @Router /remainder/{id} [PUT]
// @Summary Update Remainder
// @Description Update Remainder
// @Tags Remainder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body load_service.UpdateRemainder true "UpdateRemainderRequestBody"
// @Success 200 {object} http.Response{data=load_service.Remainder} "Remainder data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateRemainder(c *gin.Context) {
	var remainder load_service.UpdateRemainder

	remainder.Id = c.Param("id")

	err := c.ShouldBindJSON(&remainder)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.RemainderService().Update(
		c.Request.Context(),
		&remainder,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteRemainder godoc
// @ID delete_remainder
// @Router /remainder/{id} [DELETE]
// @Summary Delete Remainder
// @Description Delete Remainder
// @Tags Remainder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Remainder data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteRemainder(c *gin.Context) {

	remainderId := c.Param("id")

	resp, err := h.services.RemainderService().Delete(
		c.Request.Context(),
		&load_service.RemainderPrimaryKey{Id: remainderId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
