package handlers

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/user_service"
	"gitlab.com/market_system/ms_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Login godoc
// @ID login
// @Router /login [POST]
// @Summary Login
// @Description Login
// @Tags Login
// @Accept json
// @Procedure json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param login body user_service.LoginStaff true "LoginRequest"
// @Success 200 {object} http.Response{data=user_service.Staff} "GetStaffBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Login(c *gin.Context) {

	var logStaff user_service.LoginStaff

	err := c.ShouldBindJSON(&logStaff)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.StaffService().Login(c, &user_service.LoginStaff{
		Login:    logStaff.Login,
		Password: logStaff.Password,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	token, err := helper.GenerateJWT(map[string]interface{}{
		"staff_id": resp.Id,
	}, time.Hour*360, h.cfg.SecretKey)

	c.JSON(http.StatusOK, token)
}

// Register godoc
// @ID register
// @Router /registeration [POST]
// @Summary Register
// @Description Register
// @Tags Register
// @Accept json
// @Procedure json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param register body user_service.CreateStaff true "CreateStaffRequest"
// @Success 200 {object} http.Response{data=user_service.Staff} "GetStaffBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Register(c *gin.Context) {
	var (
		id    *user_service.Staff
		Staff user_service.CreateStaff
	)

	err := c.ShouldBindJSON(&Staff)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.StaffService().GetByID(c, &user_service.StaffPrimaryKey{Login: Staff.Login})
	fmt.Println(err.Error())
	if err != nil {
		if strings.HasSuffix(err.Error(), "no rows in result set") {
			id, err = h.services.StaffService().Create(c, &Staff)
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
		} else {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "error",
				"message": err.Error(),
			})
			return
		}
	} else if err == nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "error",
			"message": "User already exits!",
		})
		return
	}
	resp, err = h.services.StaffService().GetByID(c, &user_service.StaffPrimaryKey{Id: id.Id})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, resp)

}
