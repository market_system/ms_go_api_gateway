package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/load_service"
)

// CreateCategory godoc
// @ID create_Category
// @Router /category [POST]
// @Summary  Create Category
// @Description Create Category
// @Tags Category
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body load_service.CreateCategory true "CreateCategoryRequestBody"
// @Success 200 {object} http.Response{data=load_service.Category} "GetCategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCategory(c *gin.Context) {
	var Category load_service.CreateCategory

	err := c.ShouldBindJSON(&Category)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.CategoryService().Create(c, &Category)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetCategoryByID godoc
// @ID get_Category_by_id
// @Router /category/{id} [GET]
// @Summary Get Category By ID
// @Description Get Category By ID
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=load_service.Category} "CategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoryById(c *gin.Context) {

	categoryId := c.Param("id")

	resp, err := h.services.CategoryService().GetByID(c, &load_service.CategoryPrimaryKey{Id: categoryId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetCategoryList godoc
// @ID get_Category_list
// @Router /category [GET]
// @Summary Get Category s List
// @Description  Get Category s List
// @Tags Category
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=load_service.GetListCategoryResponse} "GetAllCategoryResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoryList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.CategoryService().GetList(
		context.Background(),
		&load_service.GetListCategoryRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateCategory godoc
// @ID update_Category
// @Router /category/{id} [PUT]
// @Summary Update Category
// @Description Update Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body load_service.UpdateCategory true "UpdateCategoryRequestBody"
// @Success 200 {object} http.Response{data=load_service.Category} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCategory(c *gin.Context) {
	var category load_service.UpdateCategory

	category.Id = c.Param("id")

	err := c.ShouldBindJSON(&category)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.CategoryService().Update(
		c.Request.Context(),
		&category,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteCategory godoc
// @ID delete_category
// @Router /category/{id} [DELETE]
// @Summary Delete Category
// @Description Delete Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCategory(c *gin.Context) {

	categoryId := c.Param("id")

	resp, err := h.services.CategoryService().Delete(
		c.Request.Context(),
		&load_service.CategoryPrimaryKey{Id: categoryId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
