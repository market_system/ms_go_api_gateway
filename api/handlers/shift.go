package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/sell_service"
)

// CreateShift godoc
// @ID create_Shift
// @Router /shift [POST]
// @Summary  Create Shift
// @Description Create Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sell_service.CreateShift true "CreateShiftRequestBody"
// @Success 200 {object} http.Response{data=sell_service.Shift} "GetShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateShift(c *gin.Context) {
	var Shift sell_service.CreateShift

	err := c.ShouldBindJSON(&Shift)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ShiftService().Create(c, &Shift)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetShiftByID godoc
// @ID get_Shift_by_id
// @Router /shift/{id} [GET]
// @Summary Get Shift By ID
// @Description Get Shift By ID
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sell_service.Shift} "ShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftById(c *gin.Context) {

	shiftId := c.Param("id")

	resp, err := h.services.ShiftService().GetByID(c, &sell_service.ShiftPrimaryKey{Id: shiftId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetShiftList godoc
// @ID get_Shift_list
// @Router /shift [GET]
// @Summary Get Shift s List
// @Description  Get Shift s List
// @Tags Shift
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=sell_service.GetListShiftResponse} "GetAllShiftResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ShiftService().GetList(
		context.Background(),
		&sell_service.GetListShiftRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateShift godoc
// @ID update_Shift
// @Router /shift/{id} [PUT]
// @Summary Update Shift
// @Description Update Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sell_service.UpdateShift true "UpdateShiftRequestBody"
// @Success 200 {object} http.Response{data=sell_service.Shift} "Shift data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateShift(c *gin.Context) {
	var shift sell_service.UpdateShift

	shift.Id = c.Param("id")

	err := c.ShouldBindJSON(&shift)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ShiftService().Update(
		c.Request.Context(),
		&shift,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteShift godoc
// @ID delete_shift
// @Router /shift/{id} [DELETE]
// @Summary Delete Shift
// @Description Delete Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Shift data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteShift(c *gin.Context) {

	shiftId := c.Param("id")

	_, err := h.services.ShiftService().Delete(
		c.Request.Context(),
		&sell_service.ShiftPrimaryKey{Id: shiftId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, "Deleted Succesfully!")
}
