package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/sell_service"
)

// CreatePayment godoc
// @ID create_Payment
// @Router /payment [POST]
// @Summary  Create Payment
// @Description Create Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sell_service.CreatePayment true "CreatePaymentRequestBody"
// @Success 200 {object} http.Response{data=sell_service.Payment} "GetPaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreatePayment(c *gin.Context) {
	var Payment sell_service.CreatePayment

	err := c.ShouldBindJSON(&Payment)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.PaymentService().Create(c, &Payment)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetPaymentByID godoc
// @ID get_Payment_by_id
// @Router /payment/{id} [GET]
// @Summary Get Payment By ID
// @Description Get Payment By ID
// @Tags Payment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sell_service.Payment} "PaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPaymentById(c *gin.Context) {

	paymentId := c.Param("id")

	resp, err := h.services.PaymentService().GetByID(c, &sell_service.PaymentPrimaryKey{Id: paymentId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetPaymentList godoc
// @ID get_Payment_list
// @Router /payment [GET]
// @Summary Get Payment s List
// @Description  Get Payment s List
// @Tags Payment
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=sell_service.GetListPaymentResponse} "GetAllPaymentResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPaymentList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.PaymentService().GetList(
		context.Background(),
		&sell_service.GetListPaymentRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdatePayment godoc
// @ID update_Payment
// @Router /payment/{id} [PUT]
// @Summary Update Payment
// @Description Update Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sell_service.UpdatePayment true "UpdatePaymentRequestBody"
// @Success 200 {object} http.Response{data=sell_service.Payment} "Payment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePayment(c *gin.Context) {
	var payment sell_service.UpdatePayment

	payment.Id = c.Param("id")

	err := c.ShouldBindJSON(&payment)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.PaymentService().Update(
		c.Request.Context(),
		&payment,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeletePayment godoc
// @ID delete_payment
// @Router /payment/{id} [DELETE]
// @Summary Delete Payment
// @Description Delete Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Payment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeletePayment(c *gin.Context) {

	paymentId := c.Param("id")

	resp, err := h.services.PaymentService().Delete(
		c.Request.Context(),
		&sell_service.PaymentPrimaryKey{Id: paymentId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
