package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/sell_service"
)

// CreateSales godoc
// @ID create_Sales
// @Router /sales [POST]
// @Summary  Create Sales
// @Description Create Sales
// @Tags Sales
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sell_service.CreateSales true "CreateSalesRequestBody"
// @Success 200 {object} http.Response{data=sell_service.Sales} "GetSalesBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSales(c *gin.Context) {
	var Sales sell_service.CreateSales

	err := c.ShouldBindJSON(&Sales)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SalesService().Create(c, &Sales)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetSalesByID godoc
// @ID get_Sales_by_id
// @Router /sales/{id} [GET]
// @Summary Get Sales By ID
// @Description Get Sales By ID
// @Tags Sales
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sell_service.Sales} "SalesBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalesById(c *gin.Context) {

	salesId := c.Param("id")

	resp, err := h.services.SalesService().GetByID(c, &sell_service.SalesPrimaryKey{Id: salesId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetSalesList godoc
// @ID get_Sales_list
// @Router /sales [GET]
// @Summary Get Sales s List
// @Description  Get Sales s List
// @Tags Sales
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=sell_service.GetListSalesResponse} "GetAllSalesResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalesList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SalesService().GetList(
		context.Background(),
		&sell_service.GetListSalesRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateSales godoc
// @ID update_Sales
// @Router /sales/{id} [PUT]
// @Summary Update Sales
// @Description Update Sales
// @Tags Sales
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sell_service.UpdateSales true "UpdateSalesRequestBody"
// @Success 200 {object} http.Response{data=sell_service.Sales} "Sales data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSales(c *gin.Context) {
	var sales sell_service.UpdateSales

	sales.Id = c.Param("id")

	err := c.ShouldBindJSON(&sales)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.SalesService().Update(
		c.Request.Context(),
		&sales,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteSales godoc
// @ID delete_sales
// @Router /sales/{id} [DELETE]
// @Summary Delete Sales
// @Description Delete Sales
// @Tags Sales
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Sales data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSales(c *gin.Context) {

	salesId := c.Param("id")

	resp, err := h.services.SalesService().Delete(
		c.Request.Context(),
		&sell_service.SalesPrimaryKey{Id: salesId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
