package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/market_system/ms_go_api_gateway/api/docs"
	"gitlab.com/market_system/ms_go_api_gateway/api/handlers"
	"gitlab.com/market_system/ms_go_api_gateway/config"
)

func SetUpAPI(r *gin.Engine, h *handlers.Handler, cfg config.Config) {

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization


	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	r.POST("login", h.Login)
	r.POST("registeration", h.Register)
	r.POST("scan", h.Scan)

	r.POST("staff", h.CreateStaff)
	r.GET("staff/:id", h.GetStaffById)
	r.GET("staff", h.GetStaffList)
	r.PUT("staff/:id", h.UpdateStaff)
	r.DELETE("staff/:id", h.DeleteStaff)

	r.POST("shift", h.CreateShift)
	r.GET("shift/:id", h.GetShiftById)
	r.GET("shift", h.GetShiftList)
	r.PUT("shift/:id", h.UpdateShift)
	r.DELETE("shift/:id", h.DeleteShift)

	v1 := r.Group("sales")

	v1.POST("", h.CreateSales)
	v1.GET("/:id", h.GetSalesById)
	v1.GET("", h.GetSalesList)
	v1.PUT("/:id", h.UpdateSales)
	v1.DELETE("/:id", h.DeleteSales)

	v1.POST("/product", h.CreateSalesProduct)
	v1.GET("/product/:id", h.GetSalesProductById)
	v1.GET("/product", h.GetSalesProductList)
	v1.PUT("/product/:id", h.UpdateSalesProduct)
	v1.DELETE("/product/:id", h.DeleteSalesProduct)

	r.POST("remainder", h.CreateRemainder)
	r.GET("remainder/:id", h.GetRemainderById)
	r.GET("remainder", h.GetRemainderList)
	r.PUT("remainder/:id", h.UpdateRemainder)
	r.DELETE("remainder/:id", h.DeleteRemainder)

	r.POST("provider", h.CreateProvider)
	r.GET("provider/:id", h.GetProviderById)
	r.GET("provider", h.GetProviderList)
	r.PUT("provider/:id", h.UpdateProvider)
	r.DELETE("provider/:id", h.DeleteProvider)

	r.POST("product", h.CreateProduct)
	r.GET("product/:id", h.GetProductById)
	r.GET("product", h.GetProductList)
	r.PUT("product/:id", h.UpdateProduct)
	r.DELETE("product/:id", h.DeleteProduct)

	r.POST("payment", h.CreatePayment)
	r.GET("payment/:id", h.GetPaymentById)
	r.GET("payment", h.GetPaymentList)
	r.PUT("payment/:id", h.UpdatePayment)
	r.DELETE("payment/:id", h.DeletePayment)

	r.POST("transaction", h.CreateTransaction)
	r.GET("transaction/:id", h.GetTransactionById)
	r.GET("transaction", h.GetTransactionList)
	r.PUT("transaction/:id", h.UpdateTransaction)
	r.DELETE("transaction/:id", h.DeleteTransaction)

	r.POST("market", h.CreateMarket)
	r.GET("market/:id", h.GetMarketById)
	r.GET("market", h.GetMarketList)
	r.PUT("market/:id", h.UpdateMarket)
	r.DELETE("market/:id", h.DeleteMarket)

	r.POST("category", h.CreateCategory)
	r.GET("category/:id", h.GetCategoryById)
	r.GET("category", h.GetCategoryList)
	r.PUT("category/:id", h.UpdateCategory)
	r.DELETE("category/:id", h.DeleteCategory)

	r.POST("brand", h.CreateBrand)
	r.GET("brand/:id", h.GetBrandById)
	r.GET("brand", h.GetBrandList)
	r.PUT("brand/:id", h.UpdateBrand)
	r.DELETE("brand/:id", h.DeleteBrand)

	r.POST("branch", h.CreateBranch)
	r.GET("branch/:id", h.GetBranchById)
	r.GET("branch", h.GetBranchList)
	r.PUT("branch/:id", h.UpdateBranch)
	r.DELETE("branch/:id", h.DeleteBranch)

	v2 := r.Group("arrival")

	v2.POST("", h.CreateArrival)
	v2.GET("/:id", h.GetArrivalById)
	v2.GET("", h.GetArrivalList)
	v2.PUT("/:id", h.UpdateArrival)
	v2.DELETE("/:id", h.DeleteArrival)

	v2.POST("/product", h.CreateArrivalProduct)
	v2.GET("/product/:id", h.GetArrivalProductById)
	v2.GET("/product", h.GetArrivalProductList)
	v2.PUT("/product/:id", h.UpdateArrivalProduct)
	v2.DELETE("/product/:id", h.DeleteArrivalProduct)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
