package services

import (
	"gitlab.com/market_system/ms_go_api_gateway/config"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/load_service"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/sell_service"
	"gitlab.com/market_system/ms_go_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	StaffService() user_service.StaffServiceClient
	BranchService() user_service.BranchServiceClient
	MarketService() user_service.MarketServiceClient

	SalesService() sell_service.SalesServiceClient
	SalesProductService() sell_service.SalesProductServiceClient
	ShiftService() sell_service.ShiftServiceClient
	PaymentService() sell_service.PaymentServiceClient
	TransactionService() sell_service.TransactionServiceClient

	ProductService() load_service.ProductServiceClient
	CategoryService() load_service.CategoryServiceClient
	RemainderService() load_service.RemainderServiceClient
	BrandService() load_service.BrandServiceClient
	ProviderService() load_service.ProviderServiceClient
	ArrivalService() load_service.ArrivalServiceClient
	ArrivalProductService() load_service.ArrivalProductServiceClient
}

type grpcClients struct {
	staff  user_service.StaffServiceClient
	branch user_service.BranchServiceClient
	market user_service.MarketServiceClient

	sales       sell_service.SalesServiceClient
	saleProduct sell_service.SalesProductServiceClient
	shift       sell_service.ShiftServiceClient
	payment     sell_service.PaymentServiceClient
	transaction sell_service.TransactionServiceClient

	product        load_service.ProductServiceClient
	category       load_service.CategoryServiceClient
	remainder      load_service.RemainderServiceClient
	brand          load_service.BrandServiceClient
	provider       load_service.ProviderServiceClient
	arrival        load_service.ArrivalServiceClient
	arrivalProduct load_service.ArrivalProductServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	connSellService, err := grpc.Dial(
		cfg.SELLServiceHost+cfg.SELLGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	connLoadService, err := grpc.Dial(
		cfg.LOADServiceHost+cfg.LOADGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		staff:          user_service.NewStaffServiceClient(connUserService),
		branch:         user_service.NewBranchServiceClient(connUserService),
		market:         user_service.NewMarketServiceClient(connUserService),
		sales:          sell_service.NewSalesServiceClient(connSellService),
		saleProduct:    sell_service.NewSalesProductServiceClient(connSellService),
		shift:          sell_service.NewShiftServiceClient(connSellService),
		payment:        sell_service.NewPaymentServiceClient(connSellService),
		transaction:    sell_service.NewTransactionServiceClient(connSellService),
		product:        load_service.NewProductServiceClient(connLoadService),
		category:       load_service.NewCategoryServiceClient(connLoadService),
		remainder:      load_service.NewRemainderServiceClient(connLoadService),
		brand:          load_service.NewBrandServiceClient(connLoadService),
		provider:       load_service.NewProviderServiceClient(connLoadService),
		arrival:        load_service.NewArrivalServiceClient(connLoadService),
		arrivalProduct: load_service.NewArrivalProductServiceClient(connLoadService),
	}, nil
}

func (g *grpcClients) StaffService() user_service.StaffServiceClient {
	return g.staff
}
func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branch
}
func (g *grpcClients) MarketService() user_service.MarketServiceClient {
	return g.market
}

func (g *grpcClients) SalesService() sell_service.SalesServiceClient {
	return g.sales
}
func (g *grpcClients) SalesProductService() sell_service.SalesProductServiceClient {
	return g.saleProduct
}
func (g *grpcClients) ShiftService() sell_service.ShiftServiceClient {
	return g.shift
}
func (g *grpcClients) PaymentService() sell_service.PaymentServiceClient {
	return g.payment
}
func (g *grpcClients) TransactionService() sell_service.TransactionServiceClient {
	return g.transaction
}

func (g *grpcClients) ProductService() load_service.ProductServiceClient {
	return g.product
}
func (g *grpcClients) CategoryService() load_service.CategoryServiceClient {
	return g.category
}
func (g *grpcClients) RemainderService() load_service.RemainderServiceClient {
	return g.remainder
}
func (g *grpcClients) BrandService() load_service.BrandServiceClient {
	return g.brand
}
func (g *grpcClients) ProviderService() load_service.ProviderServiceClient {
	return g.provider
}
func (g *grpcClients) ArrivalService() load_service.ArrivalServiceClient {
	return g.arrival
}
func (g *grpcClients) ArrivalProductService() load_service.ArrivalProductServiceClient {
	return g.arrivalProduct
}
